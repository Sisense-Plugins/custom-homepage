//	Add an object to hold the default homepages for each group
prism.customHomepage = {
	"settings": {
		"multipleGroups" : "default"								//	Can be "first","last", or "default"
	},
	"homepages": {
		"default":"57d164b48517b9b01b000022",						//	Default Dashboard
		"57d31bc0105e6e9c36000003": "57b76a3e4591399c1b000001",		//	Analysists Group
		"57d341a1105e6e9c3600006f": "57d164ae8517b9b01b00000d"		//	Sisense Group
	}
}