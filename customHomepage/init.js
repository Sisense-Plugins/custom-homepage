
prism.run([function() {
    prism.on("apploaded", function(e, args) {

    	//	Do we need to run this code?
        var needRun = (e.currentScope.appstate === 'homepage') && (prism.brand.homePage === '/plugins/customHomepage/defaultHomepage.html');
        if (needRun) {
        
            // Get all the groups a user could belong to
            var adGroups = prism.user.resolevdGroups ? prism.user.resolevdGroups : null,
                sisenseGroups = prism.user.groups ? prism.user.groups : [],
                allGroups = adGroups ? adGroups : sisenseGroups;
        
            // Find all matches in the config file
            var dashboardId = null,
                matchedHomepages = [];
            for (key in prism.customHomepage.homepages) {
                // Look for user groups that match this key
                var matchedGroup = $.grep(allGroups, function(w) {
                    return w == key;
                });
                // Was there a match
                var isMatch = (key !== "default") && (matchedGroup.length > 0);
                if (isMatch) {
                    matchedHomepages.push(prism.customHomepage.homepages[key]);
                }
            }

            // Business Logic
            if (matchedHomepages.length == 0) {
                // No match in the config file, use default
                dashboardId = prism.customHomepage.homepages.default;
            } else if (matchedHomepages.length == 1) {
                // Only 1 match, use it
                dashboardId = matchedHomepages[0];
            } else {
                // Multiple matches on groups
                var userPref = prism.customHomepage.settings.multipleGroups;
                if (userPref == "first") {
                    // Config file says to use the first match
                    dashboardId = matchedHomepages[0];
                } else if (userPref == "last") {
                    // Config file says to use the last match
                    dashboardId = matchedHomepages[matchedHomepages.length - 1];
                } else if (userPref == "default") {
                    // Config files says to use default
                    dashboardId = prism.customHomepage.homepages.default;
                } else {
                    // Catchall for other options, use default
                    dashboardId = prism.customHomepage.homepages.default;
                }
            }
        }

        if (dashboardId) {
            // Define the new url
            var newUrl = "/app/main#/dashboards/" + dashboardId;
            // Change the url
            window.location.href = newUrl;
        }
    });
}]);